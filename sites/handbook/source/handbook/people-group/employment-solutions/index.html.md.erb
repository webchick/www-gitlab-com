---
layout: handbook-page-toc
title: Employment Solutions
description: GitLab's Employment Solutions.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page contains an overview on how we have scaled team members Globally.
The DRI for all Employment Solutions is the [Employment Solutions Partner](https://about.gitlab.com/job-families/people-ops/employment-solutions). Any changes to any contracts should be requested to the Employment Solutions Partner and will require approval from our Director of Tax & our Director of Legal, Employment.

## Country Hiring Guidelines

The GitLab team has members in countries and regions all over the world and from all walks of life. Diversity, Inclusion & Belonging is one of our [core values](/handbook/values/). Due to the challenge of balancing and maintaining operating in a large number of countries, some of which have complex rules, laws, and regulations, it can affect our ability to conduct business efficiently. It unfortunately affects the employability of the citizens and residents of those countries.
We are growing and continuously expanding our hiring capabilities in a variety of geographies. 

In the past GitLab would hire in any country except those we had already determined were not feasible due to certain restrictions. At present we are focussing our hiring in countries where we have [entities](#gitlab-entities) & [PEO's](#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity). 

Compared with our former approach of hiring contractors in any country and later planning to implement an entity/PEO, we see four instant benefits with this approach:

- Delivers consistent global team member experience (e.g. payroll, benefits, support, etc.)
- Reduces operational complexity 
- Provides a stable footprint and makes it easier to grow in particular countries
- Closes the gap on building a scalable employment solution for 100% of team members

In addition, we also have the option to make an exception and consider hiring outside of these countries through the use of a PEO for future team members when supported by business needs, on a case by case basis. This approach enables a scalable solution from the onset and allows us to continue to hire diverse global talent whilst providing a consistent and positive team member experience. This approach will require a formal business need justification and will be addressed on a case by case basis.  

Countries which do not have an entity/scalable PEO will be converted as part of our [country conversion process](/handbook/people-group/employment-solutions/#country-conversions).

Details on upcoming country conversions can be viewed by GitLab team members [here](https://docs.google.com/spreadsheets/d/17e7ou1bRQn0PvpgnEUs3Yqgk6LNWTpxwCJvHuDeBbAg/edit?ts=5efb740e#gid=0).

## Team Member Types at GitLab

| Team Member Type | Abbreviation | Location |
| ---------------- | ------------ | -------- |
| Employee | Employee | Global |
| Contractor : IND | Cont-IND | Non-US |
| Contractor : C2C* | Cont-C2C | Non-US |
| Employee : PEO | Emp-PEO | Non-US |
| Contractor : PEO | Cont-PEO | Non-US |
| Consultant | Cons-US | US Only |
| Intern | Intern | Global |

- C2C means Company to Company
- IND means Independent

## Team Member Types by Country

GitLab has a few different team member types. The following tables each show how we hire and scale around the world, where we have entities, where we use Professional Employer Organisations, and how we add new countries to our list.

To ensure the accurate entry of information in BambooHR, we created this table as guidance of what is applicable in which location. When adding new team members to BambooHR, please ensure you use this terminology for accurate reporting.

More information related to payroll processes can be found on the [Payroll handbook page](/handbook/finance/payroll/).

### GitLab Entities

| Country | Team Member Type(s) | Entity | Pay Frequency | Paid By | BambooHR Profile (Y/N) |
| ------- | ------------------- | ------ | ------------- | ------- | ---------------------- |
| Australia | Employee | GitLab PTY Ltd | 12 | GitLab Payroll Dept | Y |
| Belgium | Employee | GitLab BV | 13.92 | GitLab Payroll Dept | Y |
| Canada | Employee | GitLab Canada Corp | 26 | GitLab Payroll Dept | Y |
| Germany | Employee | GitLab GmbH | 12 | GitLab Payroll Dept | Y |
| Ireland | Employee | GitLab Ireland Ltd | 12 | GitLab Payroll Dept | Y |
| Japan | Employee | GitLab GK | 12 | GitLab Payroll Dept | Y |
| Netherlands | Employee | GitLab BV | 12.96 | GitLab Payroll Dept | Y |
| New Zealand | Employee | GitLab PTY Ltd NZ (Branch of GitLab PTY Ltd Australia) | 12 | GitLab Payroll Dept | Y |
| United Kingdom | Employee | GitLab UK Ltd | 12 | GitLab Payroll Dept | Y |
| United States | Employee | GitLab Inc | 24 | GitLab Payroll Dept | Y |
| United States (Public Sector) | Employee | GitLab Federal LLC | 24 | GitLab Payroll Dept | Y |

### PEO (Professional Employer Organization/ Employer of Record and not a GitLab entity)

This table reflects where we use PEO's currently:

| Country | Agreement Type(s) | Hiring Partner | Entity to Invoice (for AP only) | Pay Frequency | Paid By | BambooHR Profile (Y/N) | Scalable PEO (Y/N) | Open for hiring (Y/N) |
| ------- | ----------------- | -------------- | ------------------------------- | ------------- | ------- | ---------------------- | ------------------ | --------------------- |
| Austria | Employed via a PEO | Remote.com | GitLab IT BV | 12 | PEO | Y | Y | Y |
| Brazil | Employed via a PEO | Safeguard | GitLab IT BV | 12 | PEO | Y | Y | **N** |
| Chile | Employed via a PEO | Global Upside | GitLab IT BV | 12 | PEO | Y | Y | Y |
| Costa Rica | Employed via a PEO | Global Upside | GitLab IT BV | 12 | PEO | Y | Y | Y |
| Czech Republic | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | **N** |
| Denmark | Employed via a PEO | Global Upside | GitLab IT BV | 12 | PEO | Y | Y | Y |
| France | Employed via a PEO | Safeguard | GitLab IT BV | 12 | PEO | Y | Y | **N** |
| Greece | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | **N** |
| Hungary | Employed via a PEO | Remote.com | GitLab IT BV | 12 | PEO | Y | Y | Y |
| India | Employed via a PEO | Global Upside | GitLab IT BV | 12 | PEO | Y | Y | Y |
| Israel | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | **N** |
| Italy | Employed via a PEO | Safeguard | GitLab IT BV | 14 | PEO | Y | Y | **N** |
| Kenya | Employed via a PEO | Global Upside | GitLab IT BV | 12 | PEO | Y | Y | Y |
| Latvia | Employed via a PEO | Global Upside | GitLab IT BV | 12 | PEO | Y | Y | Y |
| Mexico | Employed via a PEO | Remote.com | GitLab IT BV | 12 | PEO | Y | Y | Y |
| Philippines | Employed via a PEO | Global Upside | GitLab IT BV | 24 | PEO | Y | Y | Y |
| Poland | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | Y |
| Portugal | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | **N** |
| Romania | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | Y |
| Russia | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | Y |
| Serbia | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | Y |
| Slovenia | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | Y |
| South Africa | Employed via a PEO | Remote.com | GitLab IT BV | 12 | PEO | Y | Y | Y |
| South Korea | Employed via a PEO | Safeguard | GitLab IT BV | 12 | PEO | Y | Y | **N** |
| Spain | Employed via a PEO | Safeguard | GitLab IT BV | 12 | PEO | Y | Y | **N** |
| Switzerland | Employed via a PEO | Safeguard | GitLab IT BV | 12 | PEO | Y | Y | Y |
| Turkey | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | **N** |
| Ukraine | Contracted via a PEO | CXC | GitLab IT BV | 12 | PEO | Y | N | Y |

Contact information for our PEO representatives is found in the `Entity & PEO Contacts` note in the PeopleOps vault in 1Password. If a team member, manager or PBP has a question relating to a PEO, their process, or any related procedure for a PEO-employed team member, please reach out to the Employment Solutions Partner (@rhendrick) who will liaise and find a solution with the PEO's support.

### Team Members Hired as Contractors via our GitLab IT BV Entity

Where we do not have an entity or a PEO, team members contract with our GitLab IT BV entity as independent or C2C contractors.

# International Expansion

## Country Conversions

As GitLab continues to scale, we will be converting all team members to an employment scalable solution. Team members are hired in the following ways:

- Contractor through a PEO (Professional Employer Organization with a local presence)
- Employees through a PEO (Professional Employer Organization)
- GitLab entity

The Employment Solutions Partner is responsible for managing the country conversion processes outlined [here](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/issue_templates/country_conversions.md).

## Compensation Review for Country Conversions

As part of the country conversion and benefits review process, the Total Rewards and Employment Solutions Partner will review the gross value of cash compensation and benefits while also ensuring alignment to the employee rate in the compensation calculator.

The total rewards and people ops specialist team should also review alignment to the compensation ranges based on the new employee status and the range of the compensation calculator. Whenever we change salaries as part of a country conversion, these should be consistently applied among all team members in that country.

## WBSO (R&D tax credit) in the Netherlands

For roles directly relating to Research and Development in the Netherlands, GitLab may be eligible for the [WBSO (R&D Tax Credit)](http://english.rvo.nl/subsidies-programmes/wbso).

### Organizing WBSO

**Applications**

As of 2019 GitLab must submit three applications each year and the deadlines for those are as follows:

1. **31 March 2019**, for the May - August 2019 period (Product Manager for Create Features)
1. **31 August 2019**, for the September - December 2019 period (Product Manager for Gitaly)
1. **30 November 2019**, for the January - April 2020 period (Product Manager for Geo Features)

There is a [translated English version of the application template](https://docs.google.com/document/d/15B1VDL-N-FyLe84mPAMeJnSKjNouaTcNqXeKxfskskg/edit) located in the WBSO folder on the Google Drive. The applications should be completed by a Product Manager, responsible for features or a service within GitLab, who can detail the technical issues that a particular feature will solve. Assistance on completing the application can also be sought from the WBSO consultant (based in the Netherlands). The contact details for the consultant can be found in a secure note in the People Ops 1Password vault called WBSO Consultant. The People Operations Specialist team will assist with co-ordinating this process. It is currently owned by Finance.

**Hour Tracker**

Each year a spreadsheet with the project details and hours logged against the work done on the project(s) will need to be created. This is for the entire year. The current hour tracker is located in the WBSO folder on the Google Drive, and shared only with the developers that need to log their hours (located in the Netherlands), Total Rewards Analysts, Finance and the WBSO consultant. Once the projects have been completed for the year, the WBSO consultant will submit the hours and project details to claim the R&D grant from the [RVO](https://english.rvo.nl/). The WBSO consultant will contact Total Rewards Analysts should they have any queries.

