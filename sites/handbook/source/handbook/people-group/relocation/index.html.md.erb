---
layout: handbook-page-toc
title: "Relocation"
description: "GitLab's Policies & Processes in Relation to Team Member Relocations."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Relocation / Address Change

This process is managed by the People Operations Specialist team as the DRI, in partnership with Total Rewards. For any questions not addressed below, please email [peopleops@gitlab.com](mailto:peopleops@gitlab.com). 

For US based relocations (within the US) please email the People Experience [people-exp@gitlab.com](mailto:people-exp@gitlab.com) team to start the process. 

If you are considering a move, it is best to discuss this with your manager as soon as possible, always before you are thinking of moving, to understand the impact on compensation or your role at GitLab. Moving to a new location is an exciting adventure to embark upon. However, there are multiple aspects to consider and eligibility to review. 

## Importance of Communicating Location 

Generally, team members may live or work outside their permanent residence for an aggregate amount of 183 days (6 months) per year.  However, the amount does depend on the particular country (or state) and what it considers to be a "resident" for tax purposes, as well as what it considers the tax year.  If you are not sure about the aggregate amount allowed by the country or state where you are visiting, you should check with your personal tax advisor. If you believe you or a team member you manage is at risk of exceeding (or has already exceeded) the aggregate amount of 183 days (6 months) (or less in certain countries), you must contact the People group **immediately**.

If the 6 month timeframe (or other timeframe, as applicable) is at risk of being exceeded or has already been exceeded, please contact the Team Member Relations Specialist team at [teammemberrelations@gitlab.com](mailto:teammemberrelations@gitlab.com) 

Team members have the responsibility to ensure their own work eligibility by communicating their location if at risk of exceeding the 6 month timeframe, and managers have a responsibility to their team member and to the business to ensure compliance. 

Having team members for 6 months or more working outside of their contractual location poses potential tax liability for both the business and the team member. For this reason, it is of utmost importance to communicate transparently and promptly with the People Team.

**The consequences of not communicating to the appropriate group, and/or exceeding the 6-month timeframe allotment outside of an individual's country of employment could result in [underperformance](https://about.gitlab.com/handbook/leadership/underperformance/) management action.** 


### Considerations and Eligibility 

1. Does GitLab have an [entity](/handbook/people-group/employment-solutions/#gitlab-entities) or use a [PEO(Professional Employment Organisation)](/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity) in the new place you're moving to? We are currently only supporting relocations to GitLab Entities and PEOs at this time in alignment with our [Country hiring guidelines](/handbook/people-group/employment-solutions/#country-hiring-guidelines). 
1. Would this relocation change your "[Team Member Type](https://about.gitlab.com/handbook/people-group/employment-solutions/#team-member-types-at-gitlab)" with GitLab? Are you comfortable with this possible change? 
1. Do you have the appropriate right to work documentation/visa requirements in the country that you are considering relocating to? Please note, at this moment GitLab only sponsors [relocations to the Netherlands](https://about.gitlab.com/handbook/people-group/visas/#right-to-immigrate-to-the-netherlands).

Keep in mind, GitLab retains discretion at all times whether it can accomodate you to continue your role in the new location.  In some instances a move will not align to your proposed location, (e.g. a recruiter hired in EMEA to support EMEA would not be approved to move to the US), and in other instances the company may not be able to support a hire in the proposed location. Second, in almost all situations the compensation may change. For an idea about the compensation impact, please check with our Total Rewards team for an estimate.  The actual compensation amount will be reviewed and will need approval before relocation can occur.

### Short-Term Stay

If you are not changing your permanent location (where you maintain permanent residency), but instead are traveling to different locations over a period of time, you are responsible for maintaining your health insurance, visas, and any other item directly relating to your travel.

Since any short-term stay is your decision and not required by GitLab as part of your role, you will not be eligible to use the Business Accident Travel Policy or submit any expenses related to your travel. If you are hired in a role requiring a time zone alignment, you must still be able to fulfill that requirement.

If your short-term stay is for less than 6 months in a new location, there should be no need to update your address in BambooHR. If you plan on staying in a location for more than 6 months, or are in that location for an aggregate of more than 6 months in a single calendar year, that becomes your residence and you must complete the long-term relocation process described below. In most countries, tax residency is determined by the place where a GitLab Team Member keeps its vital interests. This is the place where you are registered, where your SO and/or dependents live, the place where you work, where you live, where you keep your subscriptions etc.

** Please note that in some jurisdictions, even short term stays can add up to more than 183 days per year and can subject you to payroll tax in the new location.  Anything that does not look like a temporary stay can be reviewed as staying somewhere to establish a residence for tax purposes. GitLab cannot provide you individual tax advice but you should always make sure to check with your tax advisor before traveling to any location where you will be performing work for an extended period of time, whether it is more than or less than 183 days. 

### Long-Term Relocation

#### Eligibility to Work

If you are interested in a possible long-term relocation (defined below), you need to confirm that you would be eligible to work in that location. [Except for the Netherlands](/handbook/people-group/visas/#right-to-immigrate-to-the-netherlands), GitLab does not provide any form of sponsorship or immigration aid for team members who choose to move to new countries.   For more information, please refer to our [Visa page](/handbook/people-group/visas). GitLab cannot assist or facilitate a process to help you become eligible to work in a location if you do not already have that eligibility. You are of course free to apply and gain that eligibility if there are steps you can take that do not involve GitLab.  Once you are certain that you are eligible to work in your requested Relocation country, you will need to provide proof of eligibility by uploading the proof in the Documents tab of your BambooHR profile. If you move before establishing eligibility to work, but then cannot establish eligibility to work in the new location within the first six months of residency, it may affect your ability to continue your role with GitLab.

#### Long-Term Relocation Definition

A long-term relocation means that you will establish yourself in a new location outside of your current residential area. If you are ending your current residential living arrangement, spending more than six months in one calendar year in one location as part of an extensive period of travel and/or will have your mail delivered to an address in a different city, please follow the steps below.

It is at the company's discretion to determine whether it can approve the relocation:
  - Some job positions require GitLab team members to be located in particular countries, locations, regions, or time zones.  For example, a salesperson hired to serve a specific region may need to stay in that region.
  - GitLab may not have a scalable employment solution available in the country to which you intend to relocate. Review our [Country hiring guidelines](/handbook/people-group/employment-solutions/#country-hiring-guidelines) to see if the country is eligible for a relocation. 
  - As noted above, you can only work in countries where you can establish eligibility to work in the country.
  - Relocation may also result in an adjustment to compensation based on the location and the location factor.  The Company retains discretion to determine whether it can support such an adjustment.  

Assuming that relocation is approved, the location factor of the new location may result in an adjustment to compensation.  Depending on the location factor and the benefits offered in the particular country, this adjustment may result in an increase or a decrease to compensation.
At the onset, this practice sounds harsh when moving to a lower-paid location. One might argue that it seems unfair for the organization to pay someone less for the same work in the same role, regardless of where they go. However, if you look at it from another angle for a minute and compare this practice to what most companies do, it should make more sense. For example, say you work for a company with physical locations and say they haven't accepted that remote work is as productive as coming into the office yet. If you wanted to pack up and move to a location where they did not have a physical site, you would have no alternative but to resign and seek new employment in your new location. You would find quickly that companies in the area pay at a locally competitive rate.

Now, let's say the company did have a site in your new location and they offered the flexibility to transfer. If they did not have a similar position open, you would have to either apply for a different open position in the same company or resign and apply externally (back to the realization that other companies will pay at local competitive rate). If you were lucky enough that they did have a similar role in the new location, a transfer would come with a pay rate based on the local market to ensure equity across all incumbents (people in the job) by location.

Adjusting [pay according to the local market in all cases](/handbook/total-rewards/compensation/) is fair to everyone. We can't remain consistent if we make exceptions to the policy and allow someone to make greater than local competitive rate for the same work others in that region are doing (or will be hired to do). We realize we might lose a few good people over this policy, but being fair to all team members is not negotiable. It is a value we stand behind and take very seriously.

## How To Apply for a Long-Term Relocation

#### Team Member 
- Review the [Country hiring guidelines](/handbook/people-group/employment-solutions/#country-hiring-guidelines) to see if we can support your relocation. 
_If you are moving to a different country, please start this process no later than 3 months before your ideal relocation date.  If your relocation is within the same country, please start the process no later than 30 days before your ideal relocation date._

1. Per the [Eligibility to Work](https://about.gitlab.com/handbook/people-group/relocation/#eligibility-to-work) section above, confirm that you have uploaded to BambooHR proof of your eligibility to work in your requested relocation country.
1. For US-based Team Members who are relocating within the United States, please email the People Experience Team `people-exp@gitlab.com` to start the relocation process. 
1. Submit an email to the People Operations Specialist at **peopleops@gitlab.com**. In this email include the following;
- Current Location
- New Location
- Timeframe of relocation (best guess)
- Explain your eligiblity to work in the new location 

Note: Assuming your relocation is approved, any applicable changes in compensation will not be effective until your move date (or the date you start working in the new location), which will be listed as the effective date on your new contract.


### Approvals Phase
1. People Ops will confirm eligibility, documentation, timing and any other information as applicable. If the team member is not eligible to relocate, the People Operations Specialist team will communicate this fact to the team member at this time.
-**People Ops Specialist: Please check the [Country Conversion and Entity Creation Tracking issue](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/367) to be aware of upcoming country conversions and to confirm where the Company currently hires.  These are the only countries where team members can relocate.  For certain limited situations, the Company may be able to consider a scalable employment solution in new countries.  You may open an issue and assign to employment solutions.**
- The People Operations Specialist can escalate to the Senior Manager, People Operations, Director of Tax, and Director of Legal, Employment for further review.
1. If the relocation is within the exact same geographical location and country as linked to the team member’s current location factor, no approval is needed.  Direct the team member to update their address in the Personal tab in their BambooHR profile.  The People Operations Specialist will confirm that the address has been updated in BambooHR and that there are no other steps necessary to complete the relocation process. 
1. The People Operations Specialist will request written approval from the team member's manager. After receiving confirmation from the TRM Specialists and manager that there are no existing performance concerns. If there are, follow up discussion may be needed.
1. If the relocation request relates to a _higher location factor_, forward both approvals to the team member's Division's E-Group leader for their written approval. Where applicable, copy in the Director and VP of the Division in the email to the E-Group Leader, as they are closer to the process. This is to create visibility for them and an opportunity for them to halt the relocation if need be.
1. If the relocation request relates to a _lower location factor_ or _equivalent location factor (even if it is in the same country)_, continue to the next step.
1. If the relocation is declined at any step, the People Operations Specialist will relay this first to the TMR Specialists Team, who will work with People Operations, Total Rewards, and the team member’s manager to develop a basis for the declination to explain to the team member.

### Compensation Phase
1. Once all relocation approvals have been received, start a confidential compensation thread email with the manager. In the email, include the following; 
- Current Location & Location factor
- New Location and & Location factor 
1. Once the Manager has review all factors, they will update you with the new compensation amount. 
1. The People Operations Specialist will convert the approved compensation from USD to the local pay currency of the team member's new location, use the [rounding best practice](/handbook/total-rewards/compensation/compensation-calculator/#rounding-best-practice) method, and then request approval from Total Rewards of this final compensation amount.
_Ex. Team member is approved $50,000 USD as their new comp and they are relocating to the Netherlands. Team members in the Netherlands are paid in Euro. Using the Jan 1, 2020 conversion, this converts to 44,577.20 Euro. Using the rounding best practice, the team member's comp will be 44,600 Euro, with Total Rewards approval._
1. The People Operations Specialist will forward all approvals to the Total Rewards team at total-rewards@ gitlab.com so that they may review and approve any compensation change. 
1. The Manager will convey to the team member that their relocation has been approved and relay their new compensation. 
1. If applicable, The People Operations Specialist will inform the relevant Payroll team (`uspayroll @gitlab.com` or `nonuspayroll @gitlab.com`) for visibility on any upcoming changes.
1. The People Operations Specialist will ensure that all approval documents including email threads are saved in the team member's BambooHR `Contracts & Changes` folder. This approval will include the converted salary where applicable in order for the authorised signatory on the contract / adjustment letter to audit against before signing. 
1. The People Operations Specialist will reach out to the People Experience team to prepare a [Letter of Adjustment](/handbook/people-group/promotions-transfers/#letter-of-adjustment) or new contract when applicable.

### Contract Phase 

#### Relocation within the Same Country
**People Experience Associate**

1. Create a [Letter of Adjustment](/handbook/people-group/promotions-transfers/#letter-of-adjustment), per the stated process in the Promotions & Transfers handbook page.

- A letter is only needed if there is a compensation change. If there is no change in compensation, then the People Operations Specialist should direct the team member to update their address in the Personal tab in their BambooHR profile.

- Please note that the effective date of the change is the date of the expected move for the team member. If unsure, please confirm with the team member who is relocating. 

1. Once the letter of adjustment has been created, ping the People Experience Associate or People Specialist team for auditing.
1. Stage the letter in HelloSign and send for signature first to the Senior Manager, Total Rewards and subsequently to the team member.
1. Upload the signed document to the team member's `Contracts & Changes` folder in BambooHR.
1. Make any necessary updates to the team member's BambooHR profile and email Total Rewards `total-rewards@ gitlab.com` and Payroll `uspayroll@ gitlab.com` or `nonuspayroll@ gitlab.com` to notify them of the changes.

- In the event that the team member decides not to proceed with the relocation, please delete the respective contracts from Google Drive, email and BambooHR for compliance and efficiency reasons.

**Team Member**

1. Update your address in the Personal tab in your BambooHR profile. Your address will then turn grey until it has been approved by the Total Rewards team.

**Total Rewards**

1. Confirm documentation of all necessary approvals.
1. Approve address change request in BambooHR.
1. If a compensation change is required, audit the team member's compensation details in BambooHR.
1. Update the appropriate payroll file (no need to follow this step for contractors).
1. For US team members - update the locality in BambooHR and update the benefit group if required.
1. If the team member is moving from one entity to another (certain countries have more than one entity), list them as terminated on the appropriate payroll changes file for the current entity and add them as a new hire in the appropriate payroll changes file for the new entity.
1. Check that any necessary changes to payroll and benefits administration are processed in time.

#### Relocation to a New Country
- Note: If the team member is moving to a different country with a PEO, ensure we coordinate with the new PEO about the change in contract. 
- Note: If the team member is moving to the US, employment authorisation (I-9) is required for legal purposes. 

##### Payroll set up (New Country)
- For team members relocating to a new country, it is possible there may be a delay as long as 6 weeks before their first payroll is disbursed. This is due to possible frequency changes, timing of the relocation (after the 10th of the month), availablity of new goverment documents. It is important to communicate this with the relocating team member to ensure they prepare for this possible delay. 

**People Experience Team**

1. If the team member is moving to the US, you will need to manually create an entry in LawLogix in order to complete employment verification (I-9). 
1. Create a new contract for the team member confirming new compensation (if applicable) and effective date.
- The `sent by email to:` section must have a personal email address
- No stock options are given through relocation, so that line can be removed
- Per the [Probation Period](https://about.gitlab.com/handbook/people-group/contracts-probation-periods/#probation-period) section:
_In case of relocation to a different country or change of contract, if a team member has continued service with GitLab (tenure with GitLab has been uninterrupted) and they have already passed the probationary period of their original location or contract, they do not need to go through the probation period of their new location or contract._
1. Once the contract has been created, ensure that all documentation of approvals of relocation has been uploaded to the Contracts & Changes folder of the team member's BambooHR profile. 
1. Ping the People Experience Associate, People Operations Specialist, or Manager, People Operations for auditing.
1. Stage the contract in HelloSign and send for signature first to the VP, People Ops, Technology & Analytics and subsequently to the team member.
  - In the event that the team member requests any changes to the contract, once approved and updated, send an email to the team member with the breakdown of the applicable changes once the contract has been sent for signature via HelloSign.
1. Upload the signed document to the team member's `Contracts & Changes` folder in BambooHR.
  -If the team member is relocating to the Netherlands, please see above. 
1. If applicable, create a Mutual Termination Agreement. If unsure if one is needed, check with Director, Legal - Employment at least 30 days (or as soon as possible) before the relocation takes place to confirm whether an MTA is necessary. Stage in HelloSign and send for signature first to the VP, People Ops, Technology & Analytics and subsequently to the team member. Upload the signed document to the team member's `Contracts & Changes` folder in BambooHR.
1. If a team member is leaving the US, run the command `/pops run removei9 bambooid` in Slack in order to terminate the team member in LawLogix. This will allow the I9 to be purged at the correct time. 
1. Make any necessary updates to the team member's BambooHR profile. You can follow along with [this](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/) handbook page for more detail on how to complete the changes listed below.Please note that the start date listed in the contract is the effective date of the changes you will make below.
- Fields to update in BambooHR:
  - Payroll Type: This will need to be updated closer to the time of the contract change, approximately 2 weeks before the relocation effective date. Set a calendar reminder to complete this. Also be sure to note that in your email to Total Rewards and Payroll. 
  - Compensation: Create a new Compensation line and enter the new compensation information as listed in the contract. In the comment section, note `Relocation to [Country Name] [Effective Date]` 
  - On Target Earnings: Create a new On Target Earnings line if applicable with new OTE information as listed in the contract.
  - Currency Conversion: If there is a change in currency, use our listed **Rate to USD** factor in our [Exchange Rates](https://about.gitlab.com/handbook/total-rewards/compensation/#exchange-rates) handbook section, for the current rates. We also list our current Exchange Rate Effective Date. Be sure to use [Oanda](https://www1.oanda.com/currency/converter/) to convert the currency, using the current Exchange Rate Effective Date.
  - Job Information: Create a new Job Information line with the new Effective Date and Location. Location in BambooHR is entity or PEO type, and it will be listed on the contract. You can read more in our [Employment Solutions](https://about.gitlab.com/handbook/people-group/employment-solutions/) page.
  - Additional Information: If the [pay frequency](https://about.gitlab.com/handbook/people-group/employment-solutions/) has changed you will need to add an additional line with the new frequency and effective date.
  - Employment Status: If the relocation leads to a different type of contract, such as a temporary or rolling one-year contract, you will need to add those lines in this section.
- Time Off: Review if the new country has a [PTO Accrual Policy](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#time-off-tab). 
1. Once the PEA has updated BambooHR, they will then reach out to the Total Rewards team to have them audit the updated entry.
1. Once audited, the PEA will email Total Rewards `total-rewards@ gitlab.com` and Payroll `uspayroll@ gitlab.com` or `nonuspayroll@ gitlab.com` to notify them of the changes. Be sure to mention that the Payroll type will be updated closer to the effective date.
1. Let the People Operations Specialist know that the contract has been uploaded and the changes have been made in BambooHR.
1. People Operations Specialist - Update the hiring manager and PBP in the original email thread that all tasks assigned to the DRI: People Operations Specialist relating to the relocation process are complete, for clarity. Other teams, such as Total Rewards and Payroll may still be updating their systems.

- In the event that the team member decides not to proceed with the relocation, please delete the respective contracts from Google Drive, email and BambooHR for compliance and efficiency reasons.

**Team Member**

1. Update your address in the Personal tab in your BambooHR profile. Your address will then turn grey until it has been approved by the Total Rewards team.

**Total Rewards**

1. Confirm documentation of all necessary approvals.
1. Approve address change request in BambooHR.
1. If a compensation change is required, audit the team member's compensation details in BambooHR.
1. Update the appropriate payroll file (no need to follow this step for contractors).
1. For US team members - update the locality and region (if required) in BambooHR. 
1. For US team members - Update the benefit group to INELIGIBLE and notify Lumity about the relocation with effective date.
1. If relocation is from one entity to another, list team member as terminated on the appropriate payroll changes file for the current entity and add them as a new hire in the appropriate payroll changes file for the new entity.
1. If relocation is from the US to outside of the US, change the benefit group in BambooHR to INELIGIBLE and send an email to Lumity with the termination date and coverages to be terminated.
1. If relocation is from outside the US to the US, place team member in the appropriate benefit group in BambooHR and send an email to Lumity with the relocation effective date.
1. If relocation is from outside the US to the US, please ensure the team member completes Section 1 of the I-9 on or before the first date of hire in the US and ensures that Section 2 of the I-9 is completed no later than three days after the date of hire in the US.
1. Check that any necessary changes to payroll and benefits administration are processed in time.

#### Relocating to the Netherlands 

The steps our People Operations Specialist team uses to support relocation to the Netherlands:
1. Once approval has been gained based during the [Relocation process](/handbook/people-group/relocation/#how-to-apply-for-a-long-term-relocation) and the requirements to relocate to the Netherlands have been met, a member of the People Operations Specialist team will add the approval to BambooHR under "Contracts & Changes".  
1. Next, the People Operations Specialist will reach out to our Payroll vendor in the Netherlands and cc the team member in the email to the vendor.
1. The vendor will then supply all the documentation needed to apply for relocation including information about: [30% Ruling Questionnaire](https://docs.google.com/document/d/1Ok6LS9T4P6tnPu2N6BDRDeveOYzd1ILpkbQRhl911w4/edit?ts=5caf1bca), [wage tax](https://drive.google.com/file/d/1q8N-idoYGFSCw2ajat9RscfQ004EL-PS/view) form and potentially an application for a BSN Number.
- **During COVID a letter stating your requirement to relocate to the Netherlands at such time is required, the People ops specialist will provide the vendor with such, using the following [template](https://docs.google.com/document/d/1yE7DrHBLOj0blBJwaOIC8f2bLMFS10wYUZvg9BqjnF4/edit?ts=5e592e30)** 
- **A completed health declaration form, the vendor will provide this to you**.
1. Details on that process can be found under the BSN number section on the [visas page](/handbook/people-group/visas/#bsn-number) in the handbook.
1. The People Operations Specialist team will need to review if the position is in development or research, it likely qualifies for [WBSO (R&D tax credit)](/handbook/people-group/#wbso-rd-tax-credit-in-the-netherlands); add to the WBSO hour tracker and inform our WBSO consultant. If you have any questions, feel free to ping our People Business Partner that is currently located in the Netherlands or our Director of Tax, also located in the Netherlands that can help evaluate eligibility.
1. Once the approved Visa and 30% ruling is received by the team member, the team members should email this to PeopleOps for filing in BambooHR.
1. The People Operations team should also notify the Total Rewards and Payroll team regarding the completion of the relocation.

Important:  If the team member has been with GitLab for two or more years at the time they are asking to relocate to the Netherlands, the team member should be issued an indefinite contract. 

#### Relocation Contract and Mutual Termination Agreement Process for Germany

These are the steps our People Operations Specialist team uses to send contracts to team members relocating to, or moving from Germany. All German contracts require a wet signature. As a placeholder, digital copies and signatures will be sent until our German Legal Counsel sends a copy of the wet signature documents.

Once the contract or Mututal Termination Agreement (MTA) has been created by the People Operations Specialist, next steps are to stage the documents in HelloSign, while also sending the documents to the signatories to have them physically sign and mail them. 

1. Stage the documents for siganture in HelloSign. This document will be sent to the Statutory Director first and then to the team member. Remember to use the personal email address of the team member.
  - Once signed, save the file into the team members BambooHR profile.
1. At the same time you stage the documents for signature in HelloSign, you will also need to email a copy of the document to both the Statutory Director as well as the team member.
  - In the body of the email, you will need to explain the below steps for the process.
  1. The Statutory Director will need to print two copies of the document. They will need to sign both of the copies, and then mail them to the team member. Please include the address and phone number of the team member in the email. 
  1. Once the team member receives the documents in the mail, they will keep one copy and then will need to sign and send the other copy to our German Counsel. The address for our German Counsel is listed in the PeopleOps 1password vault. Please include this address in the email.
  - The postage fees can be submitted for reimbursement. Please also include that information in the email.
  - It is also best practice to recommend in the email that the Statutory Director confirm that they have mailed the document.
1. Once the German Legal Counsel receives the documents, they will send a scanned copy of the signed documents to the People Operations team. This document will also need to be saved in the team members BambooHR profile. 

## Reporting for Relocations
At the start of each quarter the People Operations team will share a report with the People Business Partners, this report will include the following information. 
- Team Member Name
- Manager
- Relocation Date
- Starting Location
- Ending Location
- Relocation Status (Complete, In Process, Blocked)

## How To Update Address In BambooHR

* Log into BambooHR
* On the `Home` tab, click `My Info` or hover over your job title and click `View My Info`
* In the `Personal` tab, scroll down to the section where your current address appears
* Make address changes
* Click `Save Changes`

Your request will be sent to the Total Rewards team for final review and approval. Until the change is approved, which usually occurs within 24 hours, your address section will be greyed out.
