---
layout: handbook-page-toc
title: "Accounts Payable"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page contains GitLab's Accounts Payable department policies, procedures and guidelines. Our goal is to enable payments in a timely manner to both our team members and our external partners. We also provide useful links to other areas of the handbook that are relevant to the Accounts Payable procedures. 


## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Introduction

Welcome to Accounts Payable, commonly referred to as AP.  You should be able to find answers to most of your questions in the sections below. If you cannot find what you are looking for, then please connect with us:

- **Chat Channel**: `#accountspayable`
- **Email**: `ap@gitlab.com`

## <i class="fas fa-stream" id="biz-tech-icons"></i> QuickLinks

<div class="flex-row" markdown="0" style="height:110px;">
  <a href="/handbook/finance/accounting/#procure-to-pay/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span>Invoicing and Payment</span></a>  
  <a href="/handbook/finance/accounting/#credit-card-use-policy/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span>Corporate Credit Card</span></a>  
  <a href="/handbook/finance/procurement/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span style="margin-left: auto; margin-right: auto;">Procurement</span></a>
</div>

## <i class="fas fa-bullseye" id="biz-tech-icons"></i> General Guidelines

- Payment Runs are completed on Thursdays. Invoices that are selected for payment on Thursday must be fully approved and vendors successfully onboarded by end of day Tuesday of that week to be included in Thursday's payment run. The payments go out in the following days and may take 3-5 business days to reach the vendor's bank. This depends on the country, currency and the method of payment chosen. 

- Invoices are paid strictly by the invoice due dates. Any "Urgent Payment" requests will be paid in the next available payment run as long as they have been fully approved and the vendor has been successfully onboarded. AP cannot accommodate same day payment requests.

- Suppliers are to be paid via ACH and Wire only. AP does not issue check payments.

- The cutoff for customer refund payments are the 25th of each month. Any customer refund requests received after the 25th will be paid the following month.

- The AP team will only create issues that are AP related in nature. Any item forwarded to AP that is outside of our regular scope will be forwarded back to the requestor to handle or create the issue.

- Contractors and consultants who invoice bi-weekly or monthly will be set to the industry standard of 30 day payment terms.

## <i class="far fa-question-circle" id="biz-tech-icons"></i> Frequently Asked Questions

1. My invoice has not been paid yet, and my vendor is asking me when it will be paid?
   - Check if your issue or PO was fully approved. AP cannot send an invoice out for approval until ALL approvals are checked off in the procurement issue and the contract has been uploaded. Likewise, for Coupa POs, all approvals must be received before we can process an invoice against the PO.
   - Check if the invoice was sent to the correct place - an invoice uploaded to an issue will NOT get processed until it is emailed to ap@gitlab.com
   - Ensure that your vendor has fully onboarded themselves in Coupa or Tipalti.
   - Ensure that your vendor has invoiced to the correct GitLab entity. AP cannot process an invoice unless it is addressed to the entity selected in the issue or PO. If a new invoice is required, AP will reach out to the issue/PO owner and request that they obtain a new invoice from the vendor.
   - Further more, AP cannot process an invoice unless the correct VAT has been included on the invoice - per the entity that the vendor is invoicing to.
-  If you are sure that all of these things have been done correctly, please ask the vendor to contact the AP team directly, via email _ap@gitlab.com_

2. I do not know which system to push my vendor to?
   - Any vendors for Inc, Federal, IT BV and BV will be processed with a PO in Coupa as of June 2021.
   - All other entities will continue to be processed and paid through Tipalti until we implement phase 2 of Coupa, in fall of 2021. This includes Ireland, GK, GmbH, Korea, UK, Canada Corp, Singapore.
   - More information can be found on the [Procure to Pay](/handbook/finance/accounting/#procure-to-pay) page. 

3. How do I submit an expense report? And, when will it get paid?
   - All questions about expenses can be answered on the [Expense page](/handbook/finance/expenses/).
   - Reports are paid out on varying timelines depending on which country you are located in. This is also detailed on the [Expense page](/handbook/finance/expenses/).


## <i class="fas fa-file-invoice"></i> AP Processing Systems

### <i class="far fa-flag" id="biz-tech-icons"></i> Coupa is Here!

We’re excited to announce that GitLab has launched Coupa, effective June 1, 2021.

Coupa is a procure-to-pay system that will help us streamline the purchase request process, initiate workflow with approvals, and enable Purchase Orders. We will be rolling out in a phased approach, starting with the US and Netherlands entities - GitLab Inc, Federal, IT BV and BV.

All other entities will also switch over to Coupa in subsequent phases during the remainder of 2021. 

> You can learn more about Coupa in our [FAQ Page](/handbook/finance/procurement/coupa-faq/)

### <i class="far fa-flag" id="biz-tech-icons"></i> Tipalti

Tipalti is an invoice processing and payment system that GitLab has been using since 2019. We will continue to use Tipalti for invoice processing and payments for all remaining entities until phase 2 of Coupa is implemented. 

Invoices for Ireland, GK, GmbH, Korea, UK, Canada Corp, Singapore entities can be sent to *ap@gitlab.com* and/or *gitlab@supplierinvoices.com*. Vendors can also upload their invoices directly to Tipalti through their supplier portal. 

> Please visit [the Procure to Pay page](handbook/finance/accounting/#procure-to-pay) for a detailed outline of vendor onboarding and invoice processing in each system.


## <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Invoice Payments 
{: #tanuki-orange}

- The first step to getting an invoice paid is to review the approrpriate [procurement process](/handbook/finance/procurement/) to see what type of procurement issue or Coupa PO is required. 
- If the vendor is being used for the first time, we will need to invite them to onboard in either Tipalti or Coupa - depending on the entity they are billing. This instruction is also outlined in the [procurement page](/handbook/finance/procurement/). 
  - **Regardless of the system used, the procurement portion must ben compelted first.** Once that process is complete with all approvals we can process the invoice.
- Invoices should be emailed to *ap@gitlab.com*. The AP team will then process the invoice in the respective system. 

- Note that invoices are paid per the due date on the invoice or per the vendors payment terms, whichever comes first. 
- Please note that AP **cannot process same day requests for payments**. We must follow the process outlined above and ensure all approvals are attained before paying. 
- Payments are set up each Thursday and the money is released to vendors the following Monday. It may take a few days for transfers to reach vendors depending on their location and the bank they use. 
  - In order for an invoice to be included on the Thursday payment run, it must have been received and fully approved by the Tuesday. **For example**, if we are doing a payment batch on Thursday June 3rd, the invoice would have to be approved by end of day on Tuesday, June 1 in order to be included. 

> Further details on this process can be found at [the Procure to Pay page](handbook/finance/accounting/#procure-to-pay)


## <i class="fas fa-receipt" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Expenses

Please review the page on [Spending Company Money](/handbook/spending-company-money/) to ensure that your spend is within GitLab's [Expense policy](/handbook/finance/expenses/).

GitLab employs a external company called Montgomery Pacific - commonly known as Montpac. They help us to audit expenses according to the Expense Policy. You may see that they have commented on or sent a report back to you for clarifications or updates. If your report has been rejected by Montpac or Accounts Payable, and you wish to discuss this, please reach out to your manager to discuss further. They can escalate to the AP team, if required.
