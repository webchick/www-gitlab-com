---
layout: markdown_page
title: "Pipeline Validation Service Operations Working Group"
description: "Learn more about the Pipeline Validation Service Operations Working Group attributes, goals, roles and responsibilities."
canonical_path: "/company/team/structure/working-groups/pipeline-validation-service-operations/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | 2021-06-03 |
| Target End Date | 2021-12-03 |
| Slack           | #f_pipeline_validation_service (only accessible from within the company) |
| Google Doc      | [Agenda](https://docs.google.com/document/d/19-2QG0yXDt2p9vKLLxwrmqgBezk1Li7Zd6iTh-hotso/edit) (only accessible from within the company) |

## Goals

This Working Group has the following goals:

1. Operate and develop the Pipeline Validation Service (PVS) 
1. Track PVS workload and effectiveness
1. Transition PVS to a long term owner


### Exit Criteria 

1. Successfully transition PVS to a long term owner

### Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Christopher Lefelhocz | VP Development            |
| Facilitator           | Sam Goldstein         | Director Engineering, Ops |
| Development Functional Lead  | Stan Hu | Engineering Fellow |
| Security Functional Lead    | TBD | |
| Infrastructure Functional Lead  | Igor Wiedler | Staff Site Reliability Engineer |
| Product Functional Lead  | Jackie Porter | Group Manager, Product, Verify |
| Member | Wayne Haber | Director Engineering |
| Member | Grzegorz Bizon | Staff Backend Engineer, Verify |
| Member | Joanna Shih | Quality Engineering Manager, Ops |
| Member | Charl de Wit | Security Manager, Trust and Safety |
| Member | Roger Ostrander | Senior Security Engineer, Abuse Operations |
| Member | Alex Groleau | Interim Manager, Security Automation |
