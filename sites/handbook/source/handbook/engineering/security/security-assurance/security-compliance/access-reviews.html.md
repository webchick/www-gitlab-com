---
layout: handbook-page-toc
title: "Access Reviews"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Purpose

User access review process is an important control activity required for internal and external IT audits, helping to minimize threats, and provide assurance of who has access to what. This page will help provide context and guidance of access reviews for GitLab.

####  Benefits to the organization:

* Reducing risk
* Identifies dormant and/or disabled accounts
* Ensures only required team members have access to a system
* Validates users who have changed roles have not retained access no longer relevant
* Terminated team members no longer can access company systems

#### Definitions:

**Terminated Users**

* The current access listings of systems is correlated against a list of active team members in BambooHR (GitLab's source of truth for employment status). If any users are found to have active system access that are not current GitLab team members, open access removal issues to kick off the access de-provisioning process.

**Entitlement**

* Access for systems will be reviewed based on the job roles and departments. Depending on the user base size and scope of users with access, a system owner and/or manager will be involved in reviewing user entitlements. System owners should have detailed knowledge of which roles/deparments should have access to their system. A team member's manager should have detailed knowledge of their team's daily activities and whether ongoing access is needed.  If a job role is deemed to be not applicable to have access to a system, a secondary review will go the team member(s) manager to determine if access is required. In cases of dispute, a least privilege approach will be taken and the access will be requested for removal with the option of being added back in the future through the Access Request process.

## Scope

### In-Scope Systems

Security Compliance will be performing Access Reviews for Tier 1 and Tier 2 systems. See the [tech stack](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) for the current listing of Tier 1 and Tier 2 systems.

### Out-of-scope Systems

The systems captured as "in-scope" will have access reviewed by the Security Compliance team.  All other systems, containing data ranging from green to red data classifications, require system owners to review access on a quarterly basis to identify and de-provision terminated users. An entitlement review of all systems will be performed annually.

The [Access Review](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) template available in the Access Requests project provides the outline to complete these access reviews, including how to confirm [least privilege](/handbook/engineering/security/access-management-policy.html#least-privilege-reviews-for-access-requests).

In the event access is identified to no longer be required, open an [Access Removal](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue for each account that no longer requires access and related it to the system access review issue.

If you have any questions or require assistance with completing an access review, please [contact the GitLab Security Compliance team](/handbook/engineering/security/security-assurance/security-compliance/compliance.html#contact-the-compliance-team).

## Roles & Responsibilities

|  Type | In-Scope Systems | Out-of-Scope |
| :---: | :---: | :---: |
|  Terminated User | Security Compliance | System Owner |
| Entitlement | Security Compliance | System Owner |

## Access Review Cadence FY22:

- Tier 1
    - Quarterly Terminated User Access Reviews
    - Quarterly Entitlement Reviews for privileged users
    - Annual Entitlement Reviews for all users

- Tier 2
    - Annual Terminated User Access Reviews
    - Annual Entitlement Reviews for all users

All components of a user access review must be completed within the time period under audit. For example, if a user access review is scheduled for Q2, all components of the review including any required actions for modification/removal and lookbacks must be completed by the end of the quarter. It would not be sufficient to have outstanding requests for modification/removal at the quarter end, regardless of the users being identified for modification/removal prior to quarter end.

The determination and tracking of systems ranked by tiers 1-4 are managed in the GitLab Critical Systems Inventory and is the SSOT of which systems require UARs and should always be referenced when in doubt.

### Access Removals

If appropriateness of access cannot be verified as part of the review, a validation will take place with the business partner, system owner or team member’s manager prior to access removal as per the [Observation Management Procedure](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html#observation-management-procedure). If the risk associated with unvalidated access is too high, access will be revoked immediately and impacted users will be directed towards the new access request process for re provisioning. While we want to avoid disruption in access whenever possible, we need to balance the impact of that disruption with the risk of continued and unvalidated access to GitLab systems.

## Audit Requirements

#### Timing of Quarterly Access Reviews

* Access reviews are targeted to be completed within 1 month from when the system listing is exported to completion of all steps of the review. By completing the access review within a month's timeframe, we ensure the access data has not become stale. If any components of an access review are not completed within the quarter they are initiated, the review would likely be considered ineffective for compliance purposes making the month timeline all the more important to adhere to.

#### Lookback Reviews

For any accounts that require any removal of access (full removal or individual roles/privileges), a lookback review may be required. A lookback review is a review of activity for the period of time which the access was inappropriate. 

Example scenarios where a lookback may be required:
- A terminated team member is identified during the Access Review as still being an active user in a system, prior to the account being removed GitLab would perform a lookback to confirm that the terminated user did not use the access after their final day. 
- A team member was identified as having administrative access to a system that was not appropriate for their role but the user did require some level of access to the system, a lookback would be performed to validate that the administrative access was not used in an inappropriate manner.
- A team member was identified as having a role in a system which conflicted with their job responsibilities and their access in other systems allowing them to circumvent established control points and processes, a lookback would be performed to validate the user didn't use their access to circumvent established processes and controls in place.
    
In cases where there is a disagreement between system owner and manager as to whether a lookback is required, it should be completed. 
Engage the appropriate personnel (i.e system owner) to perform a lookback assessment to validate the account(s) did not use the access inappropriately.

It may not be necessary to perform a lookback in all cases, for example:
- A person transferred between non-conflicting departments, continued to support their previous role during transition, and the access review is a chance to remove the access now the transition is complete
- A role is being removed as the access granted by that role is duplicative to access granted as part of another role that will remain
- A user no longer uses the access that they have to a system but the access isn't a risk for the user to have and could be reinstated via an Access Request if a need for the access ever arises again
- A team of 4 users have the same access to a system but usage is minimal. To free up licenses and maintain an environment of least privilege, access for 3 of the users is requested for removal.

The most simple method to perform a lookback for users is to review their last login date/time and validate it was not after the date access was no longer appropriate. If a last login shows the account did authenticate after the access was inappropriate, a full review should be performed to determine any activity from the account during that time to validate no risk. If a last login is not available, other validations should be performed to confirm the account was not used inappropriately after termination (i.e review of key transactions etc.)

Evidence of the completed lookback review should be retained and documented within the access review workbook or other associated documentation

#### Validation of Modifications completed

For any accounts that are requested for modification or removal, validation they were modified as requested should be completed and evidence captured of their successful modification (i.e screenshot, updated user listing that reflects changes made).

#### Access Review Notification Reminders

Security Compliance managed access reviews required for audit evidence have a deadline of 10 business days from the creation of the access review issue.  Automated reminders will be used based on number of days out from the due date:

|  Days until Due Date | Notification | Who is Notified |
| :---: | :---: | :---: |
|  5 | Comment within access review issue | Reviewer |
|  3 | Slack ping | Reviewer |
|  2 | Comment within access review issue & <br/>Slack ping the Reviewer | Reviewer, Reviewer's Manager, and Security Compliance Manager |
|  0 | Escalated to VP of Security | VP of Security |

{-If an access review is not completed within 10 days, identified access will be removed.-}

## Access List For Review

#### Access List Generation

Based on how the system access is maintained will determine the method of account and related permissions export for access review.  This will most likely fall to the business or technical owner identified in the [Tech Stack Applications](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml).

* A system owned by Infrastructure will require an issue opened in the [Infrastructure Project](https://gitlab.com/gitlab-com/gl-infra/infrastructure) for access listing export request

##### Access List Data Fields

The following fields are the most comprehensive to assist in performing a thorough access review: (all are helpful, but all might not be available)

* Account Name
* User Name
* Email Address
* Access Permissions (level of access - user, admin,etc.)
* Account Creation Date Timestamp
* Last Access Date Timestamp

#### Access Listing Generation Validation

* If you are performing an access review, you must review the [Access Review Guidelines, including Access List Generation procedures](https://gitlab.com/gitlab-com/team-member-epics/access-requests/blob/master/README.md#additional-access-review-and-access-list-generation-guidelines) prior to starting the review process, which includes the user list generation procedures. It is important to understand the population generation requirements so completeness and accuracy of the listing and the review performed can be evidenced.

##### From the [Access Review](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Access%20Review) template:

* Please attach a system-generated access list along with evidence of how listing was produced, which should include date data was pulled. Timestamp should include the day, month, and year the listing was pulled.
* Evidence of how the listing was pulled generally takes the form of screenshots from within the system interface; the screenshots document the steps taken within the system to generate the user listing.
* If a query was used to generate the list, please provide the query along with a screenshot of system used to pull data, e.g. PostgreSQL query window, making sure screenshot evidences date/time query was run and raw row count. This will be used to evidence completeness of the population.
* To evidence completeness and accuracy, where technically feasible, include a row count or a screen shot of the row count or the generated file hash.

#### How to provide a desktop timestamp screenshot:
1. Please make sure that your workstation system clock (date and time) is visible in your screenshots.

<img src="/images/handbook/engineering/security/access-review/AR_1.png">

1. To enable this, if you are using a Mac, click on the time in the top right corner of your Mac:

<img src="/images/handbook/engineering/security/access-review/AR_2.png">

Open Date & Time Preferences and add date:

<img src="/images/handbook/engineering/security/access-review/AR_3.png">

#### How to hash files for list validation:

GCloud:
1. Open the [Google Cloud console terminal](https://console.cloud.google.com/) and use the hash command to calculate the hashes of local files `gsutil hash [-c] [-h] [-m] filename...`

<img src="/images/handbook/engineering/security/access-review/gsutil_hash.png">

2. Download the file to desktop
3. Open the Terminal app, change directory to location of the file
4. Use the md5 command to calculate the hash of the file `md5 filename...`

<img src="/images/handbook/engineering/security/access-review/terminal_hash_validation.png">

5. The values should be identical, if not, start over at step 1

## Access Review Process Next Steps

### In Progress

* Automation of in-scope systems (FY21-Q2)
    * Chef Data Bags
    * customers.gitlab.com
    * Expensify
    * Google Cloud Platform
    * NetSuite
    * Zuora

* Documentation of in-scope system permissions for entitlement review

* Development of a standard

### Future

* Continued automation of in-scope systems

* Develop process for team member manager access review by end of FY21

## Exceptions

Exceptions to this procedure will be tracked as per the [Information Security Policy Exception Management Process](/handbook/engineering/security/#information-security-policy-exception-management-process).

## References
* [Identification & Authentication Security Controls](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
* [Access Requests handbook page](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/)
* [Access Management Policy](/handbook/engineering/security/access-management-policy.html)
